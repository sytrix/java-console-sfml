package game.terminal;
import java.util.Arrays;

import org.jsfml.system.Vector2i;


public abstract class CommandSender {
	private StringBuilder stackLine;
	
	public CommandSender() {
		stackLine = new StringBuilder();
	}
	
	private String replaceFormatChar(String str) {
		if(str == null) {
			return null;
		}
		
		return str.replaceAll("&", "&&").replaceAll("�", "��");
	}
	
	private String computeString(String format, Object... args) {
		StringBuilder result = new StringBuilder();
		int ln = format.length();
		int i = 0;
		int argsIndex = 0;
		
		while(i < ln) {
			int c1 = format.codePointAt(i);
			if(c1 == '%') {
				int c2 = format.codePointAt(i + 1);
				if(c1 == c2) {
					result.appendCodePoint(c1);
				} else {
					if(c2 == 's') {
						if(argsIndex < args.length) {
							Object arg = args[argsIndex];
							if(arg == null) {
								result.append("null");
							} else {
								String str = replaceFormatChar(arg.toString());
								result.append(str);
							}
							argsIndex++;
						}
					}
				}
				
				i++;
			} else {
				result.appendCodePoint(c1);
			}
			i++;
		}
		
		return result.toString();
	}
	
	public void title(String line, Object... args) {
		String str = this.computeString(line, args);
		int x = this.getDimension().x;
		int vide = x - str.length();
		
		StringBuilder result = new StringBuilder("�5&F");
		if(vide > 0) {
			int nbSpaceBefore = vide / 2;
			int nbSpaceAfter = vide - nbSpaceBefore;
			char[] tabSpaceBefore = new char[nbSpaceBefore];
			char[] tabSpaceAfter = new char[nbSpaceAfter];
			Arrays.fill(tabSpaceBefore, ' ');
			Arrays.fill(tabSpaceAfter, ' ');
			
			result.append(tabSpaceBefore);
			result.append(str);
			result.append(tabSpaceAfter);
			
		} else {
			result.append(str);
		}
		
		this.println(result.toString());
	}
	
	public void println(String line, Object... args) {
		this.println(this.computeString(line, args));
	}
	
	public void println(String line) {
		stackLine.append(line);
		this.printOut(stackLine.toString());
		stackLine = new StringBuilder();
	}
	
	public void print(String format, Object... args) {
		this.print(this.computeString(format, args));
	}
	
	public void print(String piece) {
		String[] lines = piece.split("\n");
		
		stackLine.append(lines[0]);
		
		for(int i = 1; i < lines.length; i++) {
			this.printOut(stackLine.toString());
			stackLine = new StringBuilder();
			stackLine.append(lines[i]); 
		}
	}
	
	public void flush() {
		if(stackLine.toString().length() > 0) {
			this.printOut(stackLine.toString());
			stackLine = new StringBuilder();
		}
		this.flushOut();
	}
	
	abstract public Vector2i getDimension();
	abstract public void printOut(String line);
	abstract public void flushOut();
}
