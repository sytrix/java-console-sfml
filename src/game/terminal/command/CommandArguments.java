package game.terminal.command;

import java.util.ArrayList;
import java.util.List;

import game.terminal.CommandSender;

public abstract class CommandArguments extends Command {

	@Override
	public void execute(CommandSender sender, String cmd, String line) {
		String[] args = line.split(" ");
		List<String> listArgs = new ArrayList<>();
		
		for(String arg : args) {
			if(!arg.isEmpty()) 
			{
				listArgs.add(arg);
			}
		}
		
		this.execute(sender, cmd, listArgs);
	}
	
	public abstract void execute(CommandSender sender, String cmd, List<String> args);
	
}
