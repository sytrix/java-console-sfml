package game.terminal.command;

import java.util.HashMap;
import java.util.Map;

import game.GameManager;
import game.terminal.CommandSender;
import sytrix.parser.Parser;
import sytrix.parser.PathFindingException;
import sytrix.parser.Program;
import sytrix.parser.Token;
import sytrix.parser.langages.script.ScriptFunction;
import sytrix.parser.langages.script.ScriptMemory;


public class CommandManager {
	
	static public void execute(CommandSender sender, String cmd) {
		
		CommandList listCommand = GameManager.getCommandList();
		ScriptMemory memory = GameManager.getMemory();
		ScriptFunction functions = GameManager.getFunctions();
		Map<String, Object> bundle = new HashMap<>();
		bundle.put("memory", memory);
		bundle.put("functions", functions);
		Parser parser = GameManager.getParser();
		
		boolean findVarName = false;
		StringBuilder strVarName = new StringBuilder();
		String newCmd = cmd;
		for(int i = 0; i < cmd.length(); i++) {
			int ch = cmd.codePointAt(i);
			if(ch == '$') {
				findVarName = true;
				strVarName = new StringBuilder();
			} else {
				if(findVarName) {
					if((ch >= '0' && ch <= '9') || (ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') || ch == '_') {
						strVarName.appendCodePoint(ch);
					} else {
						Object o = memory.get(strVarName.toString());
						if(o != null) {
							newCmd = newCmd.replaceAll("\\$" + strVarName, o.toString());
						} else {
							sender.println("&5'$%s' is undefined", strVarName.toString());
						}
						findVarName = false;
					}
				}
			}
		}
		if(findVarName) {
			Object o = memory.get(strVarName.toString());
			if(o != null) {
				newCmd = newCmd.replaceAll("\\$" + strVarName, o.toString());
			} else {
				sender.println("&5'$%s' is undefined", strVarName.toString());
			}
			findVarName = false;
		}
		cmd = newCmd;
		
		if(cmd.length() > 0) {
			if(cmd.charAt(0) == '!') {
				cmd = cmd.substring(1);
				
				String key = null;
				String line = "";
				int firstSpaceIndex = cmd.indexOf(' ');
				
				if(firstSpaceIndex == -1) {
					key = cmd.toLowerCase();
				} else {
					key = cmd.substring(0, firstSpaceIndex).toLowerCase();
					line = cmd.substring(firstSpaceIndex + 1);
				}
				
				Command command = listCommand.get(key);
				if(command != null) {
					sender.println("&c>>>&7%s", cmd);
					command.execute(sender, key, line);
				} else {
					sender.println("&5Commande inconnue : %s", key);
				}
			} else {
			
				try {
					Program program = parser.compile(cmd);
					
					if(program == null) {
						sender.println("&5Erreur de compilation : " + cmd);
					} else {
						Object result = program.execute(bundle);
						sender.println("&e>>>&7%s", cmd);
						//EvalExp.evaluate(expression, functions, variables);
						if(result == null) {
							sender.println("null");
						} else {
							sender.println("%s:%s", result.getClass().getSimpleName(), result.toString());
						}
						//variables.put(varName, result);
					}
				} catch (PathFindingException e) {
					Token badToken = e.getBadToken();
					StringBuilder cursor = new StringBuilder();
					for(int i = 0; i < badToken.getIdxBeg() + 3; i++) {
						cursor.append(' ');
					}
					for(int i = 0; i < badToken.getLength(); i++) {
						cursor.append('^');
					}
					//sender.println("&5%s", badToken.toString());
					sender.println("&5>>>&7%s", cmd);
					sender.println("&5%s", cursor.toString());
					sender.println("&5Unexpected token type '%s' and value '%s'", badToken.getType(), badToken.getValue());
				} catch (Exception e) {
					for(StackTraceElement ste : e.getStackTrace()) {
						sender.println("&5%s", ste.toString());
					}
				}
			}
		}
		
		sender.flush();
	}
	
}
