package game.terminal.command;

import game.terminal.CommandSender;

public abstract class Command {
	
	abstract public void execute(CommandSender sender, String cmd, String line);
}
