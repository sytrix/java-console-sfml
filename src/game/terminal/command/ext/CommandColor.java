package game.terminal.command.ext;

import java.util.List;

import game.terminal.CommandSender;
import game.terminal.command.CommandArguments;

public class CommandColor extends CommandArguments {
	
	@Override
	public void execute(CommandSender sender, String cmd, List<String> args) {
		
		sender.println("Test de l'affichage des couleurs : ");
		sender.println("&00&11&22&33&44&55&66&77&88&99&AA&BB&CC&DD&EE&FF");
		sender.println("�00�11�22�33�44�55�66�77�88�99�AA�BB�CC�DD�EE�FF");
		sender.println("&0�00�11�22�33�44�55�66�77�88�99�AA�BB�CC�DD�EE�FF");
		sender.println("&0�0  0  �1  1  �2  2  �3  3  �4  4  �5  5  �6  6  �7  7  �8  8  �9  9  �A  A  �B  B  �C  C  �D  D  �E  E  �F  F  ");
		sender.println("&8Test affichage un peu complexe: &F�9-&5>�x.�9<&F-&5>&F-&5<&F-&5>&F�x-�3&5<&F-&5>");
		sender.println("&8Echappe les caractères : &&0 ��0");
	}
}
