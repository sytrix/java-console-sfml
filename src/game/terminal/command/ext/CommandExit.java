package game.terminal.command.ext;

import java.util.List;

import game.GameManager;
import game.terminal.CommandSender;
import game.terminal.command.CommandArguments;

public class CommandExit extends CommandArguments {

	@Override
	public void execute(CommandSender sender, String cmd, List<String> args) {
		GameManager.getRenderWindow().close();
		
	}

}
