package game.terminal.command.ext;

import java.util.ArrayList;
import java.util.List;
import game.GameManager;
import game.terminal.CommandSender;
import game.terminal.CommandSenderVoid;
import game.terminal.command.CommandArguments;
import game.terminal.command.CommandManager;
import sytrix.parser.langages.script.ScriptMemory;

public class CommandBadEval extends CommandArguments {

	private class TestEval {
		private String varName;
		private String expression;
		private Object result;
		
		public TestEval(String varName, String expression, Object result) {
			this.varName = varName;
			this.expression = expression;
			this.result = result;
		}
	}
	
	@Override
	public void execute(CommandSender sender, String cmd, List<String> args) {
		List<TestEval> listOperation = new ArrayList<>();
		
		listOperation.add(new TestEval("x", "5)*3;", new Integer(5)));
		
		
		sender.title("Test debug unexpected char : Expression");
		
		for(int i = 0; i < listOperation.size(); i++) {
			TestEval test = listOperation.get(i);
			String testCmd = test.varName + "=" + test.expression;
			CommandManager.execute(new CommandSenderVoid(), testCmd);
			ScriptMemory memory = GameManager.getMemory();
			sender.print("%s -> ", testCmd);
			Object resultCmd = memory.get(test.varName);
			String trueResult = null;
			String execResult = null;
			if(test.result != null) {
				trueResult = test.result.toString();
			}
			if(resultCmd != null) {
				execResult = resultCmd.toString();
			}
			if((resultCmd == null && test.result == null) || (resultCmd != null && resultCmd.equals(test.result))) {
				sender.println("&3OK &c(%s==%s)", trueResult, execResult);
			} else {
				sender.println("&5Fail &c(%s!=%s)", trueResult, execResult);
			}
			
		}
	}

}
