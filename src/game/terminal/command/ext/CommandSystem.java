package game.terminal.command.ext;

import java.util.List;

import org.jsfml.system.Vector2i;

import game.GameManager;
import game.ressources.SystemMode;
import game.ressources.ZoneData;
import game.terminal.CommandSender;
import game.terminal.command.CommandArguments;

public class CommandSystem extends CommandArguments {
	
	@Override
	public void execute(CommandSender sender, String cmd, List<String> args) {
		
		if(args.size() == 0) {
			sender.title("list sub command");
			sender.println("loadtileset <filepath>");
			sender.println("mode <none/tileset/gaming>");
			sender.println("savetileproperty <filename>");
			sender.println("zoom <float>");
			sender.println("info");
		} else {
			boolean nbArgumentIncorrect = false;
			
			if("loadtileset".equals(args.get(0))) {
				if(args.size() == 2) {
					String filepath = args.get(1);
					boolean isLoad = GameManager.getSystemData().loadImageTileset("assets/images/" + filepath + ".png");
					if(isLoad) {
						sender.println("&3[Done] &7tileset load %s", filepath);
					} else {
						sender.println("&5[Error] &7tileset fail to load %s", filepath);
					}
					isLoad = GameManager.getSystemData().loadTileProperty("assets/images/" + filepath + ".tp");
					if(isLoad) {
						sender.println("&3[Done] &7tileproperty load %s", filepath);
					} else {
						sender.println("&5[Error] &7tileproperty fail to load %s", filepath);
					}
					
				} else {
					nbArgumentIncorrect = true;
				}
			} else if("savetileproperty".equals(args.get(0))) {
				if(args.size() == 2) {
					String filepath = args.get(1);
					boolean isLoad = GameManager.getSystemData().saveTileProperty("assets/images/" + filepath + ".tp");
					if(isLoad) {
						sender.println("&3[Done] &7tileproperty save %s", filepath);
					} else {
						sender.println("&5[Error] &7tileproperty fail to save %s", filepath);
					}
				} else {
					nbArgumentIncorrect = true;
				}
			} else if("mode".equals(args.get(0))) {
				if(args.size() == 2) {
					String modeStr = args.get(1).toLowerCase();
					SystemMode mode = null;
					
					if("none".equals(modeStr)) {
						mode = SystemMode.NONE;
					} else if("tileset".equals(modeStr)) {
						mode = SystemMode.TILESET;
					} else if("gaming".equals(modeStr)) {
						mode = SystemMode.TILESET;
					} else if("tileproperty".equals(modeStr)) {
						mode = SystemMode.TILEPROPERTY;
					}
					
					if(mode != null) {
						GameManager.getSystemData().setMode(mode);
						sender.println("&7change mode %s", modeStr);
					} else {
						sender.println("&5change mode fail %s", modeStr);
					}
				} else {
					nbArgumentIncorrect = true;
				}
			} else if("zoom".equals(args.get(0))) {
				if(args.size() == 2) {
					float zoom = Float.parseFloat(args.get(1));
					
					GameManager.getSystemData().setZoom(zoom);
					sender.println("&7change zoom " + zoom);
				} else {
					nbArgumentIncorrect = true;
				}
			} else if("info".equals(args.get(0))) {
				if(args.size() == 1) {
					ZoneData zoneSelected = GameManager.getSystemData().getZoneSelected();
					Vector2i zoneStart = zoneSelected.getTileStart();
					Vector2i zoneEnd = zoneSelected.getTileEnd();
					
					int idStart = zoneStart.x + zoneStart.y * GameManager.getSystemData().getNbTileX();
					int idEnd = zoneEnd.x + zoneEnd.y * GameManager.getSystemData().getNbTileX();
					
					sender.println("select zone start : " + zoneStart + " (id:" + idStart + ")");
					sender.println("select zone end : " + zoneEnd + " (id:" + idEnd + ")");
				} else {
					nbArgumentIncorrect = true;
				}
			} else {
				sender.println("&5Sous commande '%s' inconnue", args.get(0));
			}
			
			if(nbArgumentIncorrect) {
				sender.println("&5Nombre de paramètre incorrecte pour la sous commande '%s'", args.get(0));
			}
		}
	}
}
