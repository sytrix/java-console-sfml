package game.terminal.command.ext;

import java.util.List;

import game.terminal.CommandSender;
import game.terminal.command.CommandArguments;

public class CommandHello extends CommandArguments {

	@Override
	public void execute(CommandSender sender, String cmd, List<String> args) {
		if(args.size() == 0) {
			sender.println("Bonjour");
		} else {
			sender.println("Bonjour %s :)", args.get(0));
		}
	}

}
