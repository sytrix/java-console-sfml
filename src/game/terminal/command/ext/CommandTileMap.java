package game.terminal.command.ext;

import java.util.List;

import game.GameManager;
import game.terminal.CommandSender;
import game.terminal.command.CommandArguments;

public class CommandTileMap extends CommandArguments {
	
	@Override
	public void execute(CommandSender sender, String cmd, List<String> args) {
		
		if(args.size() == 0) {
			sender.title("list sub command");
			sender.println("create <width> <height> [default_id] : créer une map au dimension demandée");
			sender.println("tile <x> <y> <id> : modifie un tile sur la map");
			sender.println("load <filename> : save map");
			sender.println("save <filename> : load map");
		} else {
			boolean nbArgumentIncorrect = false;
			
			if("create".equals(args.get(0))) {
				if(args.size() == 3 || args.size() == 4) {
					Integer width = Integer.parseInt(args.get(1));
					Integer height = Integer.parseInt(args.get(2));
					Integer id = 0;
					
					if(args.size() == 4) {
						id = Integer.parseInt(args.get(3));
					}
					
					GameManager.getTilemapData().create(width, height, id);
				} else {
					nbArgumentIncorrect = true;
				}
			} else if("settile".equals(args.get(0))) {
				if(args.size() == 4) {
					Integer x = Integer.parseInt(args.get(1));
					Integer y = Integer.parseInt(args.get(2));
					Integer id = Integer.parseInt(args.get(3));
					
					GameManager.getTilemapData().changeTile(x, y, id);
				} else {
					nbArgumentIncorrect = true;
				}
			} else if("load".equals(args.get(0))) {
				if(args.size() == 2) {
					String filepath = args.get(1);
					boolean isLoad = GameManager.getTilemapData().load("assets/maps/" + filepath);
					
					if(isLoad) {
						sender.println("&3[Done] &7map load : %s", filepath);
					} else {
						sender.println("&5[Error] &7map fail to load : %s", filepath);
					}
				} else {
					nbArgumentIncorrect = true;
				}
			} else if("save".equals(args.get(0))) {
				if(args.size() == 2) {
					String filepath = args.get(1);
					boolean isLoad = GameManager.getTilemapData().save("assets/maps/" + filepath);
					
					if(isLoad) {
						sender.println("&3[Done] &7map save : %s", filepath);
					} else {
						sender.println("&5[Error] &7map fail to save : %s", filepath);
					}
				} else {
					nbArgumentIncorrect = true;
				}
			} else {
				sender.println("&5Sous commande '%s' inconnue", args.get(0));
			}
			
			if(nbArgumentIncorrect) {
				sender.println("&5Nombre de paramètre incorrecte pour la sous commande '%s'", args.get(0));
			}
		}
	}
}
