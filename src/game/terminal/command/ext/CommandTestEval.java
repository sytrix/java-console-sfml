package game.terminal.command.ext;

import java.util.ArrayList;
import java.util.List;
import game.GameManager;
import game.terminal.CommandSender;
import game.terminal.CommandSenderVoid;
import game.terminal.command.CommandArguments;
import game.terminal.command.CommandManager;
import sytrix.parser.langages.script.ScriptMemory;

public class CommandTestEval extends CommandArguments {

	private class TestEval {
		private String varName;
		private String expression;
		private Object result;
		
		public TestEval(String varName, String expression, Object result) {
			this.varName = varName;
			this.expression = expression;
			this.result = result;
		}
	}
	
	@Override
	public void execute(CommandSender sender, String cmd, List<String> args) {
		List<TestEval> listOperation = new ArrayList<>();
		
		listOperation.add(new TestEval("x", "5;", new Integer(5)));
		listOperation.add(new TestEval("a", "\"A\"+x+\"BC\";", "A5BC"));
		listOperation.add(new TestEval("y", "8.653;", new Double(8.653)));
		listOperation.add(new TestEval("y", "9.546.45;", new Double(8.653)));
		listOperation.add(new TestEval("z", "x + y;", new Double(13.653)));
		listOperation.add(new TestEval("x", "3*(x);", new Integer(15)));
		listOperation.add(new TestEval("x", "pow(todouble(x), todouble(2));", new Double(15*15)));
		listOperation.add(new TestEval("x", "pow(2.53, todouble(3));", new Double(2.53*2.53*2.53)));
		listOperation.add(new TestEval("x1", "20+6*3;", new Integer(38)));
		listOperation.add(new TestEval("x1", "x1+1;", new Integer(39)));
		listOperation.add(new TestEval("x1", "x1+1;", new Integer(40)));
		listOperation.add(new TestEval("x", "(1.5+0.75*2)*(2+1);", new Double(9)));
		listOperation.add(new TestEval("x", "2.251+sqrt((1.5+0.75*2)*(2+1))*5;", new Double(17.251)));
		listOperation.add(new TestEval("x", "sin(0.2356);", Math.sin(0.2356)));
		listOperation.add(new TestEval("x", "cos(0.2357);", Math.cos(0.2357)));
		listOperation.add(new TestEval("x", "tan(0.2358);", Math.tan(0.2358)));
		listOperation.add(new TestEval("x", "46-8;", new Integer(38)));
		listOperation.add(new TestEval("x", "round(pow(sqrt(5.2), 1+(0.5*2)),3);", new Double(5.2)));
		listOperation.add(new TestEval("x", "10.0/4;", new Double(2.5)));
		//listOperation.add(new TestEval("x", "36.0/5);", null));
		listOperation.add(new TestEval("a", "\"Bonjour\";", "Bonjour"));
		listOperation.add(new TestEval("a", "\"Salut \" + x;", "Salut 2.5"));
		listOperation.add(new TestEval("b", "\"pi\" * 3;", "pipipi"));
		listOperation.add(new TestEval("a", "'petit louis';", "petit louis"));
		listOperation.add(new TestEval("b", "2 * 'l\\'arc de ' + \"robert\";", "l'arc de l'arc de robert"));
		listOperation.add(new TestEval("b", "('A');", "A"));
		listOperation.add(new TestEval("b", "3*('J');", "JJJ"));
		listOperation.add(new TestEval("b", "'A'+'B';", "AB"));
		listOperation.add(new TestEval("b", "'A'+'B'+'C';", "ABC"));
		listOperation.add(new TestEval("b", "'A'+2.0+'B'+'C';", "A2.0BC"));
		listOperation.add(new TestEval("b", "3*(3*('A')+5*('B'+'C'));", "AAABCBCBCBCBCAAABCBCBCBCBCAAABCBCBCBCBC"));
		listOperation.add(new TestEval("x", "length('salut');", new Integer(5)));
		listOperation.add(new TestEval("x", "length('sal,utation)');", new Integer(12)));
		listOperation.add(new TestEval("x", "length('sal,utat\\'ion)');", new Integer(13)));
		listOperation.add(new TestEval("x", "length(3*('sal,utat\\'ion)'));", new Integer(3 * 13)));
		listOperation.add(new TestEval("x", "3>2;", new Integer(1)));
		listOperation.add(new TestEval("x", "3-2<=2;", new Integer(1)));
		listOperation.add(new TestEval("x", "2<=2;", new Integer(1)));
		listOperation.add(new TestEval("x", "3==2;", new Integer(0)));
		listOperation.add(new TestEval("x", "3==3||2==1;", new Integer(1)));
		listOperation.add(new TestEval("x", "3==3&&2<=1;", new Integer(0)));
		
		
		sender.title("Test unitaire : Expression");
		
		for(int i = 0; i < listOperation.size(); i++) {
			TestEval test = listOperation.get(i);
			String testCmd = test.varName + "=" + test.expression;
			CommandManager.execute(new CommandSenderVoid(), testCmd);
			ScriptMemory memory = GameManager.getMemory();
			sender.print("%s -> ", testCmd);
			Object resultCmd = memory.get(test.varName);
			String trueResult = null;
			String execResult = null;
			if(test.result != null) {
				trueResult = test.result.toString();
			}
			if(resultCmd != null) {
				execResult = resultCmd.toString();
			}
			if((resultCmd == null && test.result == null) || (resultCmd != null && resultCmd.equals(test.result))) {
				sender.println("&3OK &c(%s==%s)", trueResult, execResult);
			} else {
				sender.println("&5Fail &c(%s!=%s)", trueResult, execResult);
			}
			
		}
	}

}
