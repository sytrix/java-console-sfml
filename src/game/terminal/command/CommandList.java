package game.terminal.command;

import java.util.HashMap;
import java.util.Map;

import game.terminal.command.ext.CommandBadEval;
import game.terminal.command.ext.CommandColor;
import game.terminal.command.ext.CommandExit;
import game.terminal.command.ext.CommandHello;
import game.terminal.command.ext.CommandSystem;
import game.terminal.command.ext.CommandTestEval;
import game.terminal.command.ext.CommandTileMap;
import game.terminal.command.ext.CommandWelcome;

public class CommandList {
		
	private Map<String, Command> listCommand;
	
	public CommandList() {
		this.listCommand = new HashMap<>();
		
		
		this.listCommand.put("badeval", new CommandBadEval());
		this.listCommand.put("color", new CommandColor());
		this.listCommand.put("exit", new CommandExit());
		this.listCommand.put("hello", new CommandHello());
		this.listCommand.put("testeval", new CommandTestEval());
		this.listCommand.put("tilemap", new CommandTileMap());
		this.listCommand.put("system", new CommandSystem());
		this.listCommand.put("welcome", new CommandWelcome());
	}
	
	public Command get(String command) {
		return this.listCommand.get(command);
	}
}
