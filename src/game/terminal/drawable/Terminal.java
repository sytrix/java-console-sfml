package game.terminal.drawable;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsfml.graphics.Color;
import org.jsfml.graphics.Drawable;
import org.jsfml.graphics.FloatRect;
import org.jsfml.graphics.Font;
import org.jsfml.graphics.RectangleShape;
import org.jsfml.graphics.RenderStates;
import org.jsfml.graphics.RenderTarget;
import org.jsfml.graphics.Text;
import org.jsfml.system.Clock;
import org.jsfml.system.Vector2f;
import org.jsfml.system.Vector2i;
import org.jsfml.window.Keyboard;
import org.jsfml.window.Keyboard.Key;
import org.jsfml.window.event.Event;
import org.jsfml.window.event.TextEvent;

import game.terminal.CommandSender;
import game.terminal.command.CommandManager;

public class Terminal extends CommandSender implements Drawable {

	private RectangleShape background;
	private List<String> linesStr;
	private List<OutputLine> lines;
	private List<String> inputLogStr;
	private int inputLogIndex;
	private Text input;
	private RectangleShape cursor;
	private Vector2i gridSize;
	private int cursorIndex;
	private String inputStr;
	private Vector2f position;
	private Vector2f inputPosition;
	private Vector2f terminalSize;
	private Clock cursorClock;
	private Clock tabHelpClock;
	private boolean show;
	
	Font font;
	Vector2f charSize;
	int fontSize;
	
	public Terminal(Font font, float width, float height) {
		this.font = font;
		this.fontSize = 17;
		
		this.background = new RectangleShape(new Vector2f(width, height));
		this.background.setFillColor(new Color(0, 0, 0, 220));
		//this.background.setFillColor(Color.TRANSPARENT);
		
		Text tmp = this.initText();
		tmp.setString("pl");
		FloatRect rect = tmp.getGlobalBounds();
		this.charSize = new Vector2f(rect.width / 2.f, rect.height * 1.75f);
		
		System.out.println(this.charSize.y);
		
		this.gridSize = new Vector2i((int)(width / charSize.x), (int)(height / charSize.y));
		
		this.linesStr = new ArrayList<>();
		this.lines = new ArrayList<>();
		this.inputLogStr = new ArrayList<>();
		this.inputLogIndex = 0;
		this.input = this.initText();
		this.input.setString("");
		this.cursor = new RectangleShape(new Vector2f(charSize.x / 4.f, charSize.y));
		this.cursorIndex = 0;
		this.inputStr = "";
		this.position = new Vector2f(0, 0);
		this.inputPosition = new Vector2f(0, 0);
		this.terminalSize = new Vector2f(width, height);
		this.cursorClock = new Clock();
		this.tabHelpClock = new Clock();
		this.show = false;
	}
	
	public void setSize(Vector2i size) {
		float width = size.x;
		float height = size.y;
		
		this.background.setSize(new Vector2f(width, height));
		this.gridSize = new Vector2i((int)(width / this.charSize.x), (int)(height / this.charSize.y));
		this.terminalSize = new Vector2f(width, height);
		this.inputPosition = new Vector2f(this.position.x, this.position.y + this.terminalSize.y - this.charSize.y);
		this.input.setPosition(this.inputPosition.x, this.inputPosition.y);
		this.cursor.setPosition(this.inputPosition.x + this.cursorIndex * this.charSize.x, this.inputPosition.y);
	}
	
	public boolean event(Event event) {
		
		boolean eventCatch = false;
		
		if(event.type == Event.Type.TEXT_ENTERED) {
			TextEvent textEvent = event.asTextEvent();
			//System.out.println(textEvent.unicode);
			
			if(textEvent.unicode == 339 || textEvent.unicode == 178 || textEvent.unicode == 96)
			{
				cursorClock.restart();
				this.show = !this.show;
				eventCatch = true;
			} else {
				
				if(this.show) {
					if(textEvent.unicode == 9) {
						cursorClock.restart();
						if(tabHelpClock.getElapsedTime().asMilliseconds() < 500) {
							System.out.println("double");
						} else {
							System.out.println("single");
						}
						tabHelpClock.restart();
					} else if(textEvent.unicode == 8) {
						cursorClock.restart();
						if(this.cursorIndex > 0) {
							inputStr = inputStr.substring(0, cursorIndex - 1) + inputStr.substring(cursorIndex);
							this.cursorIndex--;
						}
					} else if(textEvent.unicode == 127) {
						cursorClock.restart();
						if(inputStr.length() - this.cursorIndex > 0) {
							inputStr = inputStr.substring(0, cursorIndex) + inputStr.substring(cursorIndex + 1);
						}
					} else if(textEvent.unicode == 13) {
						cursorClock.restart();
						this.sendCommand(inputStr);
						int nbLog = this.inputLogStr.size();
						if(nbLog > 0) {
							if(this.inputLogStr.get(nbLog - 1) != inputStr) {
								this.inputLogStr.add(inputStr);
							}
						} else {
							this.inputLogStr.add(inputStr);
						}
						this.inputLogIndex = this.inputLogStr.size();
						this.inputStr = "";
						this.cursorIndex = 0;
					} else if(textEvent.unicode == 22) {
						Clipboard c = Toolkit.getDefaultToolkit().getSystemClipboard();
						try {
							String paste = (String) c.getContents(null).getTransferData(DataFlavor.stringFlavor);
							
							cursorClock.restart();
							inputStr = inputStr.substring(0, cursorIndex) + paste + inputStr.substring(cursorIndex);
							this.cursorIndex += paste.length();
							
						} catch (UnsupportedFlavorException | IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} else {
						cursorClock.restart();
						inputStr = inputStr.substring(0, cursorIndex) + (char)textEvent.unicode + inputStr.substring(cursorIndex);
						this.cursorIndex++;
					}
					
					input.setString(inputStr);
					eventCatch = true;
				}
			}
		} else if(event.type == Event.Type.KEY_PRESSED) {
			if(this.show) {
				Keyboard.Key key = event.asKeyEvent().key;
				
				if(key == Key.LEFT) {
					cursorClock.restart();
					this.cursorIndex--;
					if(this.cursorIndex < 0)
						this.cursorIndex = 0;
					eventCatch = true;
				} else if(key == Key.RIGHT) {
					cursorClock.restart();
					this.cursorIndex++;
					if(this.cursorIndex > inputStr.length())
						this.cursorIndex = inputStr.length();
					eventCatch = true;
				} else if(key == Key.UP) {
					cursorClock.restart();
					this.inputLogIndex--;
					if(this.inputLogIndex < 0) {
						this.inputLogIndex = 0;
					}
					if(this.inputLogIndex < this.inputLogStr.size()) {
						inputStr = this.inputLogStr.get(this.inputLogIndex);
					}
					input.setString(inputStr);
					cursorIndex = inputStr.length();
					eventCatch = true;
				} else if(key == Key.DOWN) {
					cursorClock.restart();
					this.inputLogIndex++;
					if(this.inputLogIndex >= this.inputLogStr.size()) {
						this.inputLogIndex = this.inputLogStr.size();
						inputStr = "";
					} else {
						inputStr = this.inputLogStr.get(this.inputLogIndex);
					}
					input.setString(inputStr);
					cursorIndex = inputStr.length();
					eventCatch = true;
				}
				
				eventCatch = true;
			}
		}
		
		cursor.setPosition(inputPosition.x + cursorIndex * charSize.x, inputPosition.y);	
		return eventCatch;
	}
	
	public void sendCommand(String str) {
		CommandManager.execute(this, str);
	}
	
	@Override
	public void printOut(String line) {
		line = "�x&7" + line;
		
		int lengthLine = line.length();
		int i = 0;
		List<String> outputLines = new ArrayList<>();
		int countPrintableChar = 0;
		int lastIndexLine = 0;
		
		while(i < (lengthLine - 1)) {
			int c1 = line.codePointAt(i);
			if(c1 == '&' || c1 == '�') {
				int c2 = line.codePointAt(i + 1);
				if(c1 == c2) {
					countPrintableChar++;
				}
				i++;
			} else {
				countPrintableChar++;
			}
			i++;
			
			if(countPrintableChar >= this.gridSize.x) {
				outputLines.add(line.substring(lastIndexLine, i));
				lastIndexLine = i + 1;
				countPrintableChar = 0;
			}
		}
		
		outputLines.add(line.substring(lastIndexLine, line.length()));
		
		for(String output : outputLines) {
			linesStr.add(output);
		}
	}
	
	@Override
	public void flushOut() {
		this.updateLines();
	}
	
	private void updateLines() {
		int nbLine = this.gridSize.y - 1;
		int index = 0;
		
		if(linesStr.size() < nbLine) {
			nbLine = linesStr.size();
		} else {
			index = linesStr.size() - nbLine;
		}
		
		lines.clear();
		
		Color lastTextColor = null;
		Color lastBackColor = null;
		
		for(int i = index; i < (index + nbLine); i++) {
			OutputLine outputLine = new OutputLine(this, lastTextColor, lastBackColor);
			outputLine.setPosition(new Vector2f(position.x, position.y + (i - index) * this.charSize.y));
			outputLine.setString(linesStr.get(i));
			lastTextColor = outputLine.getLastTextColor();
			lastBackColor = outputLine.getLastBackColor();
			this.lines.add(outputLine);
		}
	}
	
	Text initText() {
		Text text = new Text();
		text.setColor(Color.WHITE);
		text.setFont(this.font);
		text.setCharacterSize(fontSize);
		return text;
	}
	
	public void setPosition(float x, int y) {
		position = new Vector2f(x, y);
		inputPosition = new Vector2f(x, y + terminalSize.y - this.charSize.y);
		
		background.setPosition(x, y);
		input.setPosition(inputPosition.x, inputPosition.y);
		cursor.setPosition(inputPosition.x + cursorIndex * charSize.x, inputPosition.y);
		
		/*
		for(OutputLine outputLine : this.lines) {
			outputLine.setPosition(new Vector2f(x, y));
		}*/
		
		this.updateLines();
	}
	
	@Override
	public void draw(RenderTarget target, RenderStates states) {
		if(this.show) {
			target.draw(background, states);
			
			for(OutputLine line : lines) {
				target.draw(line, states);
			}
			
			target.draw(input, states);
			
			if(cursorClock.getElapsedTime().asMilliseconds() % 1000 < 500) {
				target.draw(cursor, states);
			}
		}
	}

	@Override
	public Vector2i getDimension() {
		
		return this.gridSize;
	}

	

}
