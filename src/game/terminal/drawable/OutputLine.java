package game.terminal.drawable;
import java.util.ArrayList;
import java.util.List;

import org.jsfml.graphics.Color;
import org.jsfml.graphics.Drawable;
import org.jsfml.graphics.RectangleShape;
import org.jsfml.graphics.RenderStates;
import org.jsfml.graphics.RenderTarget;
import org.jsfml.graphics.Text;
import org.jsfml.system.Vector2f;

public class OutputLine implements Drawable {
	
	private List<RectangleShape> listBack;
	private List<Text> listText;
	private Vector2f position;
	private Terminal term;
	private Color lastTextColor;
	private Color lastBackColor;
	
	public OutputLine(Terminal term, Color lastTextColor, Color lastBackColor) {
		listBack = new ArrayList<>();
		listText = new ArrayList<>();
		position = new Vector2f(0, 0);
		this.term = term;
		this.lastTextColor = lastTextColor;
		this.lastBackColor = lastBackColor;
	}
	
	public void setPosition(Vector2f newPos)
	{
		Vector2f diff = new Vector2f(newPos.x - position.x, newPos.y - position.y);
		
		for(RectangleShape back : listBack) {
			back.move(diff);
		}
		
		for(Text text : listText) {
			text.move(diff);
		}
		
		position = newPos;
	}
	
	public void setString(String str) {
		StringBuilder cumulStr = new StringBuilder();
		int lengthStr = str.length();
		int i = 0;
		int printableCount = 0;
		
		Text currentText = this.term.initText();
		currentText.setPosition(position.x, position.y);
		if(this.lastTextColor != null) {
			currentText.setColor(this.lastTextColor);
		}
		listText.add(currentText);
		
		RectangleShape currentRect = null;
		int currentRectIndexBegin = 0;
		
		if(this.lastBackColor != null) {
			currentRect = new RectangleShape(new Vector2f(0, 0));
			currentRect.setFillColor(this.lastBackColor);
			currentRect.setPosition(new Vector2f(position.x, position.y));
			listBack.add(currentRect);
		}
		
		
		while(i < (lengthStr - 1)) {
			int c1 = str.codePointAt(i);
			
			if(c1 == '&' || c1 == '�') {
				int c2 = str.codePointAt(i + 1);
				if(c1 == c2) {
					cumulStr.appendCodePoint(c1);
					printableCount++;
				} else {
					if(c1 == '&') {
						currentText.setString(cumulStr.toString());
						
						currentText = this.term.initText();
						currentText.setPosition(position.x + (printableCount * term.charSize.x), position.y);
						currentText.setColor(getColor(hexaToNb(c2)));
						listText.add(currentText);
						
						cumulStr = new StringBuilder();
					} else if(c1 == '�') {
						if(currentRect != null) {
							currentRect.setSize(new Vector2f((printableCount - currentRectIndexBegin) * term.charSize.x, term.charSize.y));
						}
						
						currentRect = new RectangleShape(new Vector2f(0, 0));
						currentRect.setFillColor(getColor(hexaToNb(c2)));
						currentRect.setPosition(new Vector2f(position.x + printableCount * term.charSize.x, position.y));
						listBack.add(currentRect);
						
						currentRectIndexBegin = printableCount;
					}
				}
				i++;
			} else {
				cumulStr.appendCodePoint(c1);
				printableCount++;
			}
			
			i++;
		}
		
		if(i < lengthStr) {
			int c1 = str.codePointAt(i);
			cumulStr.appendCodePoint(c1);
			printableCount++;
		}
		
		currentText.setString(cumulStr.toString());
		lastTextColor = currentText.getColor();
		
		if(currentRect != null) {
			currentRect.setSize(new Vector2f((printableCount - currentRectIndexBegin) * term.charSize.x, term.charSize.y));
			lastBackColor = currentRect.getFillColor();
		}
	}
	
	public Color getLastTextColor() {
		return lastTextColor;
	}
	
	public Color getLastBackColor() {
		return lastBackColor;
	}
	
	private int hexaToNb(int h) {
		if(h >= '0' && h <= '9') {
			return h - '0';
		} else if(h >= 'a' && h <= 'f') {
			return h - 'a' + 10;
		} else if(h >= 'A' && h <= 'F') {
			return h - 'A' + 10;
		} else if(h == 'x') {
			return 16;
		}
		
		return 0;
	}
	
	private Color getColor(int nb) {
		switch(nb) {
			case 0:
				return new Color(0, 0, 0);
			case 1:
				return new Color(0, 0, 255);
			case 2:
				return new Color(0, 255, 255);
			case 3:
				return new Color(0, 255, 0);
			case 4:
				return new Color(255, 255, 0);
			case 5:
				return new Color(255, 0, 0);
			case 6:
				return new Color(255, 0, 255);
			case 7:
				return new Color(170, 170, 170);
			case 8:
				return new Color(85, 85, 85);
			case 9:
				return new Color(255, 128, 0);
			case 10:
				return new Color(255, 0, 128);
			case 11:
				return new Color(128, 0, 255);
			case 12:
				return new Color(0, 128, 255);
			case 13:
				return new Color(0, 255, 128);
			case 14:
				return new Color(128, 255, 0);
			case 15:
				return new Color(255, 255, 255);
			case 16:
				return new Color(0, 0, 0, 0);
			
		}
		return null;
	}

	@Override
	public void draw(RenderTarget target, RenderStates states) {
		for(RectangleShape back : listBack) {
			target.draw(back, states);
		}
		
		for(Text text : listText) {
			target.draw(text, states);
		}
		
	}

}
