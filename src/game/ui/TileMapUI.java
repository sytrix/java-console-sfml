package game.ui;

import org.jsfml.graphics.Color;
import org.jsfml.graphics.Drawable;
import org.jsfml.graphics.IntRect;
import org.jsfml.graphics.RectangleShape;
import org.jsfml.graphics.RenderStates;
import org.jsfml.graphics.RenderTarget;
import org.jsfml.graphics.Sprite;
import org.jsfml.graphics.Texture;
import org.jsfml.system.Vector2f;
import org.jsfml.system.Vector2i;
import org.jsfml.window.Mouse;
import org.jsfml.window.event.Event;
import org.jsfml.window.event.MouseButtonEvent;
import org.jsfml.window.event.MouseEvent;

import game.GameManager;
import game.data.SystemData;
import game.data.TileMapData;
import game.ressources.SystemMode;
import game.ressources.TileData;
import game.ressources.ZoneData;

public class TileMapUI implements Drawable {
		
	private TileMapData tilemap;
	private SystemData system;
	private boolean mouseLeftPressed;
	private boolean mouseRightPressed;
	private Vector2i mousePosition;
	
	public TileMapUI() {
		this.tilemap = GameManager.getTilemapData();
		this.system = GameManager.getSystemData();
		this.mouseLeftPressed = false;
		this.mousePosition = null;
	}
	
	private void putZone(int originX, int originY) {
		ZoneData selectionZone = this.system.getZoneSelected();
		Vector2i zoneStart = selectionZone.getTileStart();
		Vector2i zoneSize = selectionZone.getZoneSize();
		int nbTileX = this.system.getNbTileX();
		
		for(int x = 0; x < zoneSize.x; x++) {
			for(int y = 0; y < zoneSize.y; y++) {
				int id = (zoneStart.x + x) + (zoneStart.y + y) * nbTileX;
				tilemap.changeTile(originX + x, originY + y, id);
			}
		}
	}
	
	public boolean event(Event event) {
		
		if(this.system.getMode() == SystemMode.TILEPROPERTY) {
			return false;
		}
		
		boolean eventCatch = false;
		
		int tilesize = this.system.getTilesize();
		float zoom = this.system.getZoom();
		int tilesetShift = 0;
		
		if(this.system.getMode() == SystemMode.TILESET) {
			tilesetShift = (int)(this.system.getTextureSizeX() * zoom);
		}
		
		
		int tilerectsize = (int)(zoom * tilesize);
		
		if(event.type == Event.Type.MOUSE_BUTTON_PRESSED) {
			MouseButtonEvent mouseButtonEvent = event.asMouseButtonEvent();
			int x = (mouseButtonEvent.position.x - tilesetShift) / tilerectsize;
			int y = mouseButtonEvent.position.y / tilerectsize;
			
			if(mouseButtonEvent.button.equals(Mouse.Button.LEFT)) {
				if(x < this.tilemap.getWidth() && y < this.tilemap.getHeight() && x >= 0 && y >= 0) {
					this.putZone(x, y);
					
					this.mouseLeftPressed = true;
					eventCatch = true;
				}
			} else if(mouseButtonEvent.button.equals(Mouse.Button.RIGHT)) {
				if(x < this.tilemap.getWidth() && y < this.tilemap.getHeight() && x >= 0 && y >= 0) {
					this.tilemap.changeTile(x, y, this.tilemap.getDefaultId());
					
					this.mouseRightPressed = true;
					eventCatch = true;
				}
			} else if(mouseButtonEvent.button.equals(Mouse.Button.MIDDLE)) {
				if(x < this.tilemap.getWidth() && y < this.tilemap.getHeight() && x >= 0 && y >= 0) {
					int id = this.tilemap.getTile(x, y);
					this.system.setTileSelected(id);
					eventCatch = true;
				}
			}
		} else if(event.type == Event.Type.MOUSE_BUTTON_RELEASED) {
			MouseButtonEvent mouseButtonEvent = event.asMouseButtonEvent();
			int x = (mouseButtonEvent.position.x - tilesetShift) / tilerectsize;
			int y = mouseButtonEvent.position.y / tilerectsize;
			
			if(mouseButtonEvent.button.equals(Mouse.Button.LEFT)) {
				if(x < this.tilemap.getWidth() && y < this.tilemap.getHeight() && x >= 0 && y >= 0) {
					//this.putZone(x, y);
					
					this.mouseLeftPressed = false;
					eventCatch = true;
				}
				
				this.mouseLeftPressed = false;
			} else if(mouseButtonEvent.button.equals(Mouse.Button.RIGHT)) {
				if(x < this.tilemap.getWidth() && y < this.tilemap.getHeight() && x >= 0 && y >= 0) {
					//this.tilemap.changeTile(x, y, this.tilemap.getDefaultId());
					
					this.mouseRightPressed = false;
					eventCatch = true;
				}
			}
		} else if(event.type == Event.Type.MOUSE_MOVED) {
			MouseEvent mouseEvent = event.asMouseEvent();
			int x = (mouseEvent.position.x - tilesetShift) / tilerectsize;
			int y = mouseEvent.position.y / tilerectsize;
			
			if(x < this.tilemap.getWidth() && y < this.tilemap.getHeight() && x >= 0 && y >= 0) {
				this.mousePosition = new Vector2i(x, y);
			} else {
				this.mousePosition = null;
			}
			
			if(this.mouseLeftPressed) {
				
				if(x < this.tilemap.getWidth() && y < this.tilemap.getHeight() && x >= 0 && y >= 0) {
					this.putZone(x, y);
				}
				
				eventCatch = true;
			}
			if(this.mouseRightPressed) {
				this.tilemap.changeTile(x, y, this.tilemap.getDefaultId());
			}
		}
		
		return eventCatch;
	}

	@Override
	public void draw(RenderTarget renderTarget, RenderStates renderStates) {
		
		if(this.system.getMode() != SystemMode.TILEPROPERTY) {
			TileData[][] tilemap = this.tilemap.getTileMap();
			Texture texture = this.system.getImageTileset();
			
			if(tilemap != null && texture != null) {
				
				int width = this.tilemap.getWidth();
				int height = this.tilemap.getHeight();
				int tilesize = this.system.getTilesize();
				Sprite sprite = new Sprite(texture);
				float zoom = this.system.getZoom();
				int tilesetShift = 0;
				
				if(this.system.getMode() == SystemMode.TILESET) {
					tilesetShift = (int)(this.system.getTextureSizeX() * zoom);
				}
				
				sprite.scale(zoom, zoom); 
				int tilerectsize = (int)(zoom * (tilesize));
				
				for(int x = 0; x < width; x++) {
					for(int y = 0; y < height; y++) {
						TileData tile = tilemap[x][y];
						
						int rectX = tile.id % 20;
						int rectY = tile.id / 20;
						sprite.setPosition(tilesetShift + x * tilerectsize, y * tilerectsize);
						sprite.setTextureRect(new IntRect(1 + rectX * 17, 1 + rectY * 17, 16, 16));
						
						renderTarget.draw(sprite, renderStates);
					}
				}
				
				if(this.system.getMode() == SystemMode.TILESET) {
					
					if(this.mousePosition != null) {
						RectangleShape tiletoplace = new RectangleShape(new Vector2f(tilerectsize, tilerectsize));
						ZoneData selectionZone = this.system.getZoneSelected();
						
						//Vector2i zoneStart = selectionZone.getTileStart();
						Vector2i zoneSize = selectionZone.getZoneSize();
						tiletoplace.setFillColor(new Color(0, 0, 255, 128));
						
						for(int x = 0; x < zoneSize.x; x++) {
							for(int y = 0; y < zoneSize.y; y++) {
								
								tiletoplace.setPosition(tilesetShift + (this.mousePosition.x + x) * tilerectsize, (this.mousePosition.y + y) * tilerectsize);
								
								renderTarget.draw(tiletoplace, renderStates);
							}
						}
					}
				}
			}
		}
	}
}

