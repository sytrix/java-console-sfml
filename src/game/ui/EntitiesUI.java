package game.ui;

import java.util.Iterator;

import org.jsfml.graphics.Drawable;
import org.jsfml.graphics.RenderStates;
import org.jsfml.graphics.RenderTarget;
import org.jsfml.graphics.Sprite;
import org.jsfml.system.Vector2f;
import org.jsfml.system.Vector2i;
import org.jsfml.window.Keyboard;
import org.jsfml.window.Keyboard.Key;
import org.jsfml.window.event.Event;

import game.GameManager;
import game.data.EntitiesData;
import game.data.SystemData;
import game.ressources.EntityData;
import game.ressources.EntityMode;
import game.ressources.PlayerData;
import game.ressources.SystemMode;

public class EntitiesUI implements Drawable {

	private SystemData system;
	private EntitiesData entities;
	private boolean pressUp;
	private boolean pressDown;
	private boolean pressLeft;
	private boolean pressRight;
	private boolean pressFire;
	
	public EntitiesUI() {
		this.system = GameManager.getSystemData();
		this.entities = GameManager.getEntitiesData();
		this.pressUp = false;
		this.pressDown = false;
		this.pressLeft = false;
		this.pressRight = false;
		this.pressFire = false;
	}
	
	public boolean event(Event event) {
		
		boolean eventCatch = false;
		
		if(event.type == Event.Type.KEY_PRESSED) {
			Keyboard.Key key = event.asKeyEvent().key;
			
			if(key == Key.LEFT) {
				this.pressLeft = true;
				eventCatch = true;
			} else if(key == Key.RIGHT) {
				this.pressRight = true;
				eventCatch = true;
			} else if(key == Key.UP) {
				this.pressUp = true;
				eventCatch = true;
			} else if(key == Key.DOWN) {
				this.pressDown = true;
				eventCatch = true;
			} else if(key == Key.SPACE) {
				this.pressFire = true;
				eventCatch = true;
			}
		} else if(event.type == Event.Type.KEY_RELEASED) {
			Keyboard.Key key = event.asKeyEvent().key;
			
			if(key == Key.LEFT) {
				this.pressLeft = false;
				eventCatch = true;
			} else if(key == Key.RIGHT) {
				this.pressRight = false;
				eventCatch = true;
			} else if(key == Key.UP) {
				this.pressUp = false;
				eventCatch = true;
			} else if(key == Key.DOWN) {
				this.pressDown = false;
				eventCatch = false;
			} else if(key == Key.SPACE) {
				this.pressFire = false;
				eventCatch = true;
			}
		}
 		
		return eventCatch;
	}
	
	public void update() {
		Iterator<EntityData> iterator = entities.getList().iterator(); 
		
		while(iterator.hasNext()) {
			EntityData entity = iterator.next();
			
			if(entity instanceof PlayerData) {
				PlayerData playerData = (PlayerData)entity;
				boolean buttonPress = false;
				int npx = 0;
				int npy = 0;
				int orientation = -1;
				
				if(!this.pressFire) {
					if(!(this.pressUp && this.pressDown)) {
						if(this.pressUp) {
							npy = -1;
						} else if(this.pressDown) {
							npy = 1;
						}
					}
					
					if(!(this.pressLeft && this.pressRight)) {
						if(this.pressLeft) {
							npx = -1;
						} else if(this.pressRight) {
							npx = 1;
						}
					}
				
					if(npx == 0 && npy == -1) {
						orientation = 0;
					} else if(npx == 1 && npy == -1) {
						orientation = 1;
					} else if(npx == 1 && npy == 0) {
						orientation = 2;
					} else if(npx == 1 && npy == 1) {
						orientation = 3;
					} else if(npx == 0 && npy == 1) {
						orientation = 4;
					} else if(npx == -1 && npy == 1) {
						orientation = 5;
					} else if(npx == -1 && npy == 0) {
						orientation = 6;
					} else if(npx == -1 && npy == -1) {
						orientation = 7;
					}
				}
				
				if(this.pressFire) {
					playerData.setMode(EntityMode.FIRE);
				} else if(npx == 0 && npy == 0) {
					playerData.setMode(EntityMode.IDLE);
				} else {
					playerData.setMode(EntityMode.WALK);
				}
				
				if(orientation != -1) {
					playerData.setOrientation(orientation);
				}
			}
			
			entity.update();
		}
	}
	
	@Override
	public void draw(RenderTarget target, RenderStates states) {
		Iterator<EntityData> iterator = entities.getList().iterator(); 
		
		int tilesize = this.system.getTilesize();
		float zoom = this.system.getZoom();
		float tilesetShift = 0;
		
		if(this.system.getMode() == SystemMode.TILESET) {
			tilesetShift = (int)(this.system.getTextureSizeX() * zoom);
		}
		
		while(iterator.hasNext()) {
			EntityData entity = iterator.next();
			Sprite sprite = entity.getSprite();
			Vector2i entityPosition = entity.getPosition();
			
			sprite.setPosition(new Vector2f(tilesetShift + entityPosition.x * zoom, entityPosition.y * zoom));
			sprite.setScale(zoom, zoom);
			
			target.draw(sprite);
		}
	}

}
