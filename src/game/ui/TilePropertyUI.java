package game.ui;

import org.jsfml.graphics.Color;
import org.jsfml.graphics.Drawable;
import org.jsfml.graphics.IntRect;
import org.jsfml.graphics.RectangleShape;
import org.jsfml.graphics.RenderStates;
import org.jsfml.graphics.RenderTarget;
import org.jsfml.graphics.Sprite;
import org.jsfml.graphics.Texture;
import org.jsfml.system.Vector2f;
import org.jsfml.system.Vector2i;
import org.jsfml.window.event.Event;
import org.jsfml.window.event.MouseButtonEvent;

import game.GameManager;
import game.data.SystemData;
import game.ressources.SystemMode;
import game.ressources.TileProperty;

public class TilePropertyUI implements Drawable {
	
	private SystemData system;
	private int zoomFactor;
	
	public TilePropertyUI() {
		this.system = GameManager.getSystemData();
		this.zoomFactor = 10;
	}
	
	public boolean event(Event event) {
		boolean eventCatch = false;
		
		if(this.system.getMode() == SystemMode.TILEPROPERTY) {
			int tilesize = this.system.getTilesize();
			float zoom = this.system.getZoom();
			int tilesetShift = (int)(this.system.getTextureSizeX() * zoom);
			TileProperty tileProperty = this.system.getTilePropertySelected();
			float scaleOfTheSprite = zoom * this.zoomFactor;
			float sizeOfTheSprite = scaleOfTheSprite * tilesize;
			float sizeOfBorder = zoom * this.zoomFactor;
			
			if(event.type == Event.Type.MOUSE_BUTTON_PRESSED) {
				MouseButtonEvent mouseButtonEvent = event.asMouseButtonEvent();
				Vector2i pos = mouseButtonEvent.position;
				int px = pos.x - tilesetShift;
				int py = pos.y;
				
				if(px >= 0 && px < sizeOfTheSprite && py >= 0 && py < sizeOfTheSprite) {
					
					if(px < sizeOfBorder) {
						tileProperty.wallLeft = !tileProperty.wallLeft;
					}
					if(px > sizeOfTheSprite - sizeOfBorder) {
						tileProperty.wallRight = !tileProperty.wallRight;
					} 
					if(py < sizeOfBorder) {
						tileProperty.wallUp = !tileProperty.wallUp;
					}
					if(py > sizeOfTheSprite - sizeOfBorder) {
						tileProperty.wallDown = !tileProperty.wallDown;
					} 
					
					eventCatch = true;
				}
			}
			
		}
		
		return eventCatch;
	}
	
	@Override
	public void draw(RenderTarget target, RenderStates states) {
		
		if(this.system.getMode() == SystemMode.TILEPROPERTY) {
			
			Texture texture = this.system.getImageTileset();
			
			if(texture != null) {
				int tileID = this.system.getIDSelected();
				TileProperty tileProperty = this.system.getTilePropertySelected();
				int tilesize = this.system.getTilesize();
				float zoom = this.system.getZoom();
				int tilesetShift = (int)(this.system.getTextureSizeX() * zoom);
				float scaleOfTheSprite = zoom * this.zoomFactor;
				float sizeOfTheSprite = scaleOfTheSprite * tilesize;
				float sizeOfBorder = zoom * this.zoomFactor;
				int rectX = tileID % 20;
				int rectY = tileID / 20;
				
				Sprite sprite = new Sprite(texture);
				
				sprite.setPosition(new Vector2f(tilesetShift, 0));
				sprite.scale(scaleOfTheSprite, scaleOfTheSprite);
				sprite.setTextureRect(new IntRect(1 + rectX * (tilesize + 1), 1 + rectY * (tilesize + 1), tilesize, tilesize));
				
				target.draw(sprite);
				
				RectangleShape wallUp = new RectangleShape();
				RectangleShape wallDown = new RectangleShape();
				RectangleShape wallLeft = new RectangleShape();
				RectangleShape wallRight = new RectangleShape();
				
				wallUp.setSize(new Vector2f(sizeOfTheSprite, sizeOfBorder));
				wallDown.setSize(new Vector2f(sizeOfTheSprite, sizeOfBorder));
				wallLeft.setSize(new Vector2f(sizeOfBorder, sizeOfTheSprite));
				wallRight.setSize(new Vector2f(sizeOfBorder, sizeOfTheSprite));
				
				wallUp.setFillColor(new Color(0, 255, 0));
				wallDown.setFillColor(new Color(0, 255, 0));
				wallLeft.setFillColor(new Color(0, 255, 0));
				wallRight.setFillColor(new Color(0, 255, 0));
				
				wallUp.setPosition(new Vector2f(tilesetShift, 0));
				wallDown.setPosition(new Vector2f(tilesetShift, sizeOfTheSprite - sizeOfBorder));
				wallLeft.setPosition(new Vector2f(tilesetShift, 0));
				wallRight.setPosition(new Vector2f(tilesetShift + sizeOfTheSprite - sizeOfBorder, 0));
				
				if(tileProperty.wallUp) {
					target.draw(wallUp);
				}
				if(tileProperty.wallDown) {
					target.draw(wallDown);
				}
				if(tileProperty.wallLeft) {
					target.draw(wallLeft);
				}
				if(tileProperty.wallRight) {
					target.draw(wallRight);
				}
			}
		}
		
	}

}
