package game.ui;

import org.jsfml.graphics.Color;
import org.jsfml.graphics.Drawable;
import org.jsfml.graphics.RectangleShape;
import org.jsfml.graphics.RenderStates;
import org.jsfml.graphics.RenderTarget;
import org.jsfml.graphics.Sprite;
import org.jsfml.graphics.Texture;
import org.jsfml.system.Vector2f;
import org.jsfml.system.Vector2i;
import org.jsfml.window.Mouse;
import org.jsfml.window.event.Event;
import org.jsfml.window.event.MouseButtonEvent;
import org.jsfml.window.event.MouseEvent;

import game.GameManager;
import game.data.SystemData;
import game.ressources.SystemMode;
import game.ressources.ZoneData;

public class TileSetUI implements Drawable {
	
	private SystemData system;
	private boolean mouseLeftPressed;
	
	public TileSetUI() {
		this.system = GameManager.getSystemData();
		this.mouseLeftPressed = false;
	}
	
	public boolean event(Event event) {
		
		boolean eventCatch = false;
		
		if(this.system.getMode() == SystemMode.TILESET || this.system.getMode() == SystemMode.TILEPROPERTY) {
			ZoneData selectionZone = this.system.getZoneSelected();
			
			int tilesize = this.system.getTilesize();
			float zoom = this.system.getZoom();
			int tilerectsize = (int)(zoom * (tilesize + 1.f));
			
			if(event.type == Event.Type.MOUSE_BUTTON_PRESSED) {
				MouseButtonEvent mouseButtonEvent = event.asMouseButtonEvent();
				
				if(mouseButtonEvent.button.equals(Mouse.Button.LEFT)) {
					int x = mouseButtonEvent.position.x / tilerectsize;
					int y = mouseButtonEvent.position.y / tilerectsize;
					
					if(x < this.system.getNbTileX() && y < this.system.getNbTileY() && x >= 0 && y >= 0) {
						selectionZone.setTileA(x, y);
						selectionZone.setTileB(x, y);
						
						if(this.system.getMode() != SystemMode.TILEPROPERTY) {
							this.mouseLeftPressed = true;
						}
						eventCatch = true;
					}
				}
			} else if(event.type == Event.Type.MOUSE_BUTTON_RELEASED) {
				MouseButtonEvent mouseButtonEvent = event.asMouseButtonEvent();
				
				if(mouseButtonEvent.button.equals(Mouse.Button.LEFT)) {
						if(this.mouseLeftPressed) {
						int x = mouseButtonEvent.position.x / tilerectsize;
						int y = mouseButtonEvent.position.y / tilerectsize;
						
						if(x < this.system.getNbTileX() && y < this.system.getNbTileY() && x >= 0 && y >= 0) {
							selectionZone.setTileB(x, y);
						}
						
						
						this.mouseLeftPressed = false;
					}
				}
			} else if(event.type == Event.Type.MOUSE_MOVED) {
				MouseEvent mouseEvent = event.asMouseEvent();
				
				if(this.mouseLeftPressed) {
					int x = mouseEvent.position.x / tilerectsize;
					int y = mouseEvent.position.y / tilerectsize;
					
					if(x < this.system.getNbTileX() && y < this.system.getNbTileY() && x >= 0 && y >= 0) {
						selectionZone.setTileB(x, y);
					}
					
					eventCatch = true;
				}
			}
		}
		
		return eventCatch;
	}

	@Override
	public void draw(RenderTarget renderTarget, RenderStates renderStates) {
		
		if(this.system.getMode() == SystemMode.TILESET || this.system.getMode() == SystemMode.TILEPROPERTY) {
			
			Texture texture = this.system.getImageTileset();
			
			if(texture != null) {
				ZoneData selectionZone = this.system.getZoneSelected();
				Sprite sprite = new Sprite(texture);
				int tilesize = this.system.getTilesize();
				float zoom = this.system.getZoom();
				int tilerectsize = (int)(zoom * (tilesize + 1.f));
				RectangleShape rect = new RectangleShape();
				
				Vector2i tileStart = selectionZone.getTileStart();
				Vector2i selectionSize = selectionZone.getZoneSize();
				
				
				rect.setFillColor(Color.TRANSPARENT);
				rect.setOutlineColor(Color.BLUE);
				rect.setOutlineThickness(zoom * 2);
				rect.setPosition(new Vector2f(tileStart.x * tilerectsize + zoom, tileStart.y * tilerectsize + zoom));
				rect.setSize(new Vector2f(selectionSize.x * tilerectsize - zoom, selectionSize.y * tilerectsize - zoom));
				
				sprite.scale(zoom, zoom); 
			
				renderTarget.draw(sprite, renderStates);
				renderTarget.draw(rect, renderStates);
			}
		}
		
	}
}

