package game;
import java.io.IOException;
import java.nio.file.Paths;

import org.jsfml.graphics.Color;
import org.jsfml.graphics.Font;
import org.jsfml.graphics.RenderWindow;
import org.jsfml.system.Vector2i;
import org.jsfml.window.VideoMode;
import org.jsfml.window.event.Event;
import org.jsfml.window.event.SizeEvent;

import game.ressources.EntityMode;
import game.ressources.PlayerData;
import game.terminal.drawable.Terminal;
import game.ui.EntitiesUI;
import game.ui.TileMapUI;
import game.ui.TilePropertyUI;
import game.ui.TileSetUI;

public class Game {
	
	static public void main(String[] args) {
		Game game = new Game();
		game.run();
	}
	
	private RenderWindow win;
	
	
	public void run() {
		
		// constante resolution fenêtre
		int winW = 1200;
		int winH = 800;
		
		// créer la fenêtre
		win = new RenderWindow(new VideoMode(winW, winH), "SFML");
		win.setFramerateLimit(60);
		GameManager.setRenderWindow(win);
		
		// charge une autre police
		Font fontJackInput = new Font();
		try {
			fontJackInput.loadFromFile(Paths.get("assets/fonts/consola.ttf"));
		} catch (IOException e1) {
		}
		
		// créer un objet terminal qui utilisera la police
		float termHeightRatio = 1.0f;
		int termHeight = (int) (winH * termHeightRatio);
		Terminal term = new Terminal(fontJackInput, winW, termHeight);
		term.setPosition(0, winH - termHeight);
		term.sendCommand("!color");
		//term.sendCommand("!tilemap create 30 20 226");
		term.sendCommand("!system loadtileset tilemap");
		term.sendCommand("!system mode tileset");
		term.sendCommand("!tilemap load test2");
		term.sendCommand("!system zoom 2.3");
		term.sendCommand("!badeval");
		//term.sendCommand("!color");
		
		// la je crée mes deux éléments tilemap et son camarades tileset
		TileMapUI tilemap = new TileMapUI();
		TileSetUI tileset = new TileSetUI();
		TilePropertyUI tileproperty = new TilePropertyUI();
		EntitiesUI entities = new EntitiesUI();
		boolean first = true;
		
		PlayerData player = new PlayerData(new Vector2i(16, 32), 0, 5);
		player.setMode(EntityMode.WALK);
		GameManager.getEntitiesData().add(player);
		
		//win.setSize(new Vector2i(2000, 3000));
		
		// tant que la fenêtre est ouverte faire...
		while(win.isOpen()) {
			
			// boucle d'évenements clavier, souris, fenêtre
			Iterable<Event> events = win.pollEvents();
			for(Event e : events) {
				
				// si on appuie sur la petite croix de la fenêtre :)
				if(e.type == Event.Type.CLOSED) {
					win.close();
				}
				// si on change la taille de la fenetre, la recrée au même dimension en la remettant en position
				if(e.type == Event.Type.RESIZED) {
					SizeEvent sizeEvent = e.asSizeEvent();
					Vector2i position = win.getPosition();
					if(!first) {
						termHeight = (int) (sizeEvent.size.y * termHeightRatio);
						win.close();
						win.create(new VideoMode(sizeEvent.size.x, sizeEvent.size.y), "SFML");
						win.setPosition(position);
						term.setPosition(0, sizeEvent.size.y - termHeight);
						term.setSize(new Vector2i(sizeEvent.size.x, termHeight));
						first = true;
					} else {
						first = false;
					}
				}
				
				// faire parvenir les évenements au terminal sinon a la tileset sinon à la tilemap 
				if(!term.event(e)) {
					if(!tileset.event(e)) {
						if(!tilemap.event(e)) {
							if(!tileproperty.event(e)) {
								if(!entities.event(e)) {
								}
							}
						}
					}
				}
			}
			
			// update les entités pour quels prennent leur nouveaux états
			entities.update();
			
			// efface l'écran puis affiche un rectangle gris foncé, puis un text "Hello World" puis le terminal par dessus
			win.clear(new Color(16, 16, 16));
			
			//win.draw(rectangle);
			//win.draw(text);
			win.draw(tilemap);
			win.draw(entities);
			win.draw(tileset);
			win.draw(tileproperty);
			win.draw(term);
			
			// update de l'affichageœ
			win.display();
		}
		
		
		
		
	}
}
