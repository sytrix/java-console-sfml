package game;

import org.jsfml.graphics.RenderWindow;

import game.data.EntitiesData;
import game.data.SystemData;
import game.data.TileMapData;
import game.terminal.command.CommandList;
import sytrix.parser.Parser;
import sytrix.parser.langages.script.ScriptFunction;
import sytrix.parser.langages.script.ScriptMemory;
import sytrix.parser.langages.script.ScriptParser;

public class GameManager {
	
	static private RenderWindow win = null;
	static private TileMapData tilemapData = new TileMapData();
	static private SystemData systemData = new SystemData();
	static private EntitiesData entitiesData = new EntitiesData();
	static private ScriptMemory memory = new ScriptMemory(null);
	static private ScriptFunction functions = new ScriptFunction();
	static private Parser parser = new ScriptParser();
	static private CommandList commandList = new CommandList();
	
	public static void setRenderWindow(RenderWindow win) {
		GameManager.win = win;
	}
	public static RenderWindow getRenderWindow() {
		return win;
	}
	public static TileMapData getTilemapData() {
		return tilemapData;
	}
	public static SystemData getSystemData() {
		return systemData;
	}
	public static ScriptMemory getMemory() {
		return memory;
	}
	public static ScriptFunction getFunctions() {
		return functions;
	}
	public static Parser getParser() {
		return parser;
	} 
	public static CommandList getCommandList() {
		return commandList;
	}
	public static EntitiesData getEntitiesData() {
		return entitiesData;
	}
	
	
	
	

}
