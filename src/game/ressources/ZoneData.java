package game.ressources;

import org.jsfml.system.Vector2i;

public class ZoneData {
	
	private Vector2i tileA;
	private Vector2i tileB;
	
	public ZoneData() {

		this.tileA = new Vector2i(0, 0);
		this.tileB = new Vector2i(0, 0);
	}
	
	public void setTileA(int x, int y) {
		this.tileA = new Vector2i(x, y);
	}
	
	public void setTileB(int x, int y) {
		this.tileB = new Vector2i(x, y);
	}
	
	public Vector2i getTileStart() {
		int x = 0;
		int y = 0;
		
		if(tileA.x < tileB.x) {
			x = tileA.x;
		} else {
			x = tileB.x;
		}
		
		if(tileA.y < tileB.y) {
			y = tileA.y;
		} else {
			y = tileB.y;
		}
		
		return new Vector2i(x, y);
	}
	
	public Vector2i getTileEnd() {
		int x = 0;
		int y = 0;
		
		if(tileA.x > tileB.x) {
			x = tileA.x;
		} else {
			x = tileB.x;
		}
		
		if(tileA.y > tileB.y) {
			y = tileA.y;
		} else {
			y = tileB.y;
		}
		
		return new Vector2i(x, y);
	}
	
	public Vector2i getZoneSize() {
		Vector2i tileStart = this.getTileStart();
		Vector2i tileEnd = this.getTileEnd();
		int selSizeX = tileEnd.x - tileStart.x + 1;
		int selSizeY = tileEnd.y - tileStart.y + 1;
		
		Vector2i selectionSize = new Vector2i(selSizeX, selSizeY);
		
		return selectionSize;
	}
}
