package game.ressources;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.jsfml.graphics.Color;
import org.jsfml.graphics.Image;
import org.jsfml.graphics.IntRect;
import org.jsfml.graphics.Sprite;
import org.jsfml.graphics.Texture;
import org.jsfml.graphics.TextureCreationException;
import org.jsfml.system.Vector2f;

public enum EntityType {
	PLAYER,
	;
	
	private Texture texture;
	private EntityFrame entityFrame;
	
	private EntityType() {
		texture = null;
	}
	
	public boolean load() {
		if(this.texture == null) {
			String filename = "";
			if(this.equals(EntityType.PLAYER)) {
				filename = "game_character.png";
				entityFrame = new EntityFrame(32, 32, 8, 6, 1, 1);
			}
			
			Path path = Paths.get("assets/images/" + filename);
			Image image = new Image();
			try {
				image.loadFromFile(path);
				
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}
			image.createMaskFromColor(new Color(255, 0, 255));
			
			this.texture = new Texture();
			try {
				this.texture.loadFromImage(image);
			} catch (TextureCreationException e) {
				e.printStackTrace();
				return false;
			}
		}
		
		return true;
	}
	
	public Sprite getIdle(int orientation, int character) {
		if(this.equals(EntityType.PLAYER)) {
			int originx;
			int originy;
			
			if(character % 2 == 0) {
				originx = 11;
			} else {
				originx = 11 + 21 + (32 + 1) * 8;
			}
			
			originy = 9 + (character / 2) * (22 + (32 + 1) * 6); 
			
			// DEBUG
			//orientation = (int) (System.currentTimeMillis() / 1000) % 8;
			
			IntRect frame = entityFrame.getRectWithOrigin(originx, originy, orientation);
					
			Sprite res = new Sprite(this.texture);
			res.setTextureRect(frame);
			res.setOrigin(this.getOrigin());
			
			return res;		
		}
		
		return null;
	}
	
	public Sprite getWalk(int orientation, int walkcounter, int character) {
		if(this.equals(EntityType.PLAYER)) {
			int originx;
			int originy;
			
			if(character % 2 == 0) {
				originx = 11;
			} else {
				originx = 11 + 21 + (32 + 1) * 8;
			}
			
			originy = 9 + (character / 2) * (22 + (32 + 1) * 6); 
			
			// DEBUG
			//walkcounter = Math.abs((int)(System.currentTimeMillis() / 150) % 4);
			//orientation = Math.abs((int)(System.currentTimeMillis() / (4 * 5 * 150)) % 8);
			
			IntRect frame = entityFrame.getRectWithOrigin(originx, originy, 8 + 4 * orientation + walkcounter % 4);
			
			Sprite res = new Sprite(this.texture);
			res.setTextureRect(frame);
			res.setOrigin(this.getOrigin());
			
			return res;		
		}
		
		return null;
	}
	
	public Sprite getFire(int orientation, int character) {
		System.out.println("fine");
		if(this.equals(EntityType.PLAYER)) {
			int originx;
			int originy;
			
			if(character % 2 == 0) {
				originx = 11;
			} else {
				originx = 11 + 21 + (32 + 1) * 8;
			}
			
			originy = 9 + (character / 2) * (22 + (32 + 1) * 6); 
			
			// DEBUG
			//orientation = Math.abs((int)(System.currentTimeMillis() / 1000) % 8);
			
			IntRect frame = entityFrame.getRectWithOrigin(originx, originy, 8 * 5 + orientation);
			
			Sprite res = new Sprite(this.texture);
			res.setTextureRect(frame);
			res.setOrigin(this.getOrigin());
			
			return res;	
		}
		
		return null;
	}
	
	public Vector2f getOrigin() {
		return entityFrame.getOrigin();
	}
}
