package game.ressources;

import org.jsfml.graphics.IntRect;
import org.jsfml.system.Vector2f;

public class EntityFrame {
	private int sizex;
	private int sizey;
	private int nbx;
	private int nby;
	private int gsx;
	private int gsy;
	
	public EntityFrame(int sizex, int sizey, int nbx, int nby, int gsx, int gsy) {
		this.sizex = sizex;
		this.sizey = sizey;
		this.nbx = nbx;
		this.nby = nby;
		this.gsx = gsx;
		this.gsy = gsy;
	}
	
	public IntRect getRect(int index) {
		int x = index % nbx;
		int y = index / nbx;
		
		return new IntRect((sizex + gsx) * x, (sizey + gsy) * y, sizex, sizey);
	}
	
	public IntRect getRectWithOrigin(int originx, int originy, int index) {
		int x = index % nbx;
		int y = index / nbx;
		
		return new IntRect(originx + (sizex + gsx) * x, originy + (sizey + gsy) * y, sizex, sizey);
	}
	
	public Vector2f getOrigin() {
		return new Vector2f(sizex / 2, sizey);
	}
}
