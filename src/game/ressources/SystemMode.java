package game.ressources;

public enum SystemMode {
	
	NONE,
	TILESET,
	GAMING,
	TILEPROPERTY,
}
