package game.ressources;

import org.jsfml.graphics.Sprite;
import org.jsfml.system.Vector2i;

public class EntityData {
	protected EntityType type;
	protected Vector2i position;
	protected int orientation;
	protected int walkcount;
	protected EntityMode mode;
	protected long lastWalkTime;
	
	public EntityData(EntityType type, Vector2i position, int orientation) {
		this.type = type;
		this.position = position;
		this.orientation = orientation;
		this.walkcount = 0;
		this.mode = EntityMode.IDLE;
		this.lastWalkTime = 0;
		
		type.load();
	}
	
	public EntityType getType() {
		return this.type;
	}
	
	public Vector2i getPosition() {
		return this.position;
	}
	
	public int getOrientation() {
		return this.orientation;
	}
	
	public void setOrientation(int orientation) {
		this.orientation = orientation;
	}
	
	public void setMode(EntityMode mode) {
		
		if(!this.mode.equals(mode) && mode.equals(EntityMode.WALK)) {
			this.lastWalkTime = System.currentTimeMillis();
			this.walkcount = 0;
		}
		
		this.mode = mode;
	}
	
	public void update() {
		long updateWalkTime = 150;
		if(mode.equals(EntityMode.WALK)) {
			if(System.currentTimeMillis() - this.lastWalkTime > updateWalkTime) {
				this.lastWalkTime += updateWalkTime;
				this.walkcount++;
			}
		}
	}

	public Sprite getSprite() { 
		System.out.println("mod");
		if(mode.equals(EntityMode.IDLE)) {
			return this.type.getIdle(this.orientation, 0);
		} else if(mode.equals(EntityMode.WALK)) {
			return this.type.getWalk(this.orientation, walkcount, 0);
		} else if(mode.equals(EntityMode.FIRE)) {
			return this.type.getFire(this.orientation, 0);
		}
		
		return null;
	}
	
}
