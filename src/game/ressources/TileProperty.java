package game.ressources;

public class TileProperty {
	
	public boolean wallUp;
	public boolean wallDown;
	public boolean wallRight;
	public boolean wallLeft;
	
	public TileProperty() {
		this.wallUp = false;
		this.wallDown = false;
		this.wallRight = false;
		this.wallLeft = false;
	}
}
