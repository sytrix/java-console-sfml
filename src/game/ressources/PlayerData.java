package game.ressources;

import org.jsfml.graphics.Sprite;
import org.jsfml.system.Vector2i;

public class PlayerData extends EntityData {
	private int character;
	
	public PlayerData(Vector2i position, int orientation, int character) {
		super(EntityType.PLAYER, position, orientation);
		
		this.character = character;
	}
	
	public int getCharacter() {
		return this.character;
	}
	
	@Override
	public Sprite getSprite() {
		
		int character = this.getCharacter();
		if(mode.equals(EntityMode.IDLE)) {
			return this.type.getIdle(this.orientation, character);
		} else if(mode.equals(EntityMode.WALK)) {
			return this.type.getWalk(this.orientation, walkcount, character);
		} else if(mode.equals(EntityMode.IDLE)) {
			return this.type.getFire(this.orientation, character);
		}
		
		return null;
	}
}
