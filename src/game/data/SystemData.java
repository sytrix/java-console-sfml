package game.data;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.jsfml.graphics.Texture;
import org.jsfml.system.Vector2i;

import game.ressources.SystemMode;
import game.ressources.TileProperty;
import game.ressources.ZoneData;

public class SystemData {
	
	private int tilesize;
	private int nbTileX;
	private int nbTileY;
	private float zoom;
	private Texture imageTileset;
	private TileProperty[] tileproperty;
	private SystemMode mode;
	private ZoneData zoneSelected;
	
	public SystemData() {
		this.tilesize = 16;
		this.nbTileX = 20;
		this.nbTileY = 19;
		this.zoom = 2;
		this.imageTileset = null;
		this.mode = SystemMode.NONE;
		this.zoneSelected = new ZoneData();
		
		int nbTile = this.nbTileX * this.nbTileY;
		this.tileproperty = new TileProperty[nbTile];
		for(int i = 0; i < nbTile; i++) {
			this.tileproperty[i] = new TileProperty();
		}
	}
	
	public int getNbTileX() {
		return this.nbTileX;
	}
	
	public int getNbTileY() {
		return this.nbTileY;
	}
	
	public int getTilesize() {
		return this.tilesize;
	}
	
	public void setZoom(float zoom) {
		this.zoom = zoom;
	}
	
	public float getZoom() {
		return this.zoom;
	}
	
	public boolean loadImageTileset(String filepath) {
		Texture texture = new Texture();
		Path path = Paths.get(filepath);
		
		if(!Files.exists(path)) {
			return false;
		}
		
		try {
			texture.loadFromFile(path);
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		
		this.imageTileset = texture;
		return true;
	}
	
	public Texture getImageTileset() {
		return this.imageTileset;
	}
	
	public float getTextureSizeX() {
		if(this.imageTileset == null) {
			return 0.f;
		}
		
		return this.imageTileset.getSize().x;
	}
	
	public SystemMode getMode() {
		return this.mode;
	}
	
	public void setMode(SystemMode mode) {
		this.mode = mode;
		
		if(this.mode == SystemMode.TILEPROPERTY) {
			Vector2i start = this.zoneSelected.getTileStart();
			this.zoneSelected.setTileA(start.x, start.y);
			this.zoneSelected.setTileB(start.x, start.y);
		}
	}
	
	public ZoneData getZoneSelected() {
		return this.zoneSelected;
	}
	
	public int getIDSelected() {
		Vector2i tile = this.zoneSelected.getTileStart();
		int id = tile.x + tile.y * this.nbTileX;
		return id;
	}
	
	public TileProperty getTilePropertySelected() {
		int id = this.getIDSelected();
		return this.tileproperty[id];
	}
	
	public void setTileSelected(int id) {
		int x = id % this.nbTileX;
		int y = id / this.nbTileX;
		this.zoneSelected.setTileA(x, y);
		this.zoneSelected.setTileB(x, y);
	}
	
	public boolean loadTileProperty(String filepath) {
		
		Path path = Paths.get(filepath);
		
		if(!Files.exists(path)) {
			return false;
		}
		
		DataInputStream in = null;
		
		try {
			in = new DataInputStream(new FileInputStream(filepath));
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return false;
		}
		
		try {
			int nbTile = this.nbTileX * this.nbTileY;
			for(int i = 0; i < nbTile; i++) {
				this.tileproperty[i].wallUp = in.readBoolean();
				this.tileproperty[i].wallDown = in.readBoolean();
				this.tileproperty[i].wallLeft = in.readBoolean();
				this.tileproperty[i].wallRight = in.readBoolean();
			}
			
			in.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	    
	    
	    return true;
	}
	
	public boolean saveTileProperty(String filepath) {
		
		DataOutputStream out = null;
		
		try {
			out = new DataOutputStream(new FileOutputStream(filepath));
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return false;
		}
		
		try {
			int nbTile = this.nbTileX * this.nbTileY;
			for(int i = 0; i < nbTile; i++) {
				out.writeBoolean(this.tileproperty[i].wallUp);
				out.writeBoolean(this.tileproperty[i].wallDown);
				out.writeBoolean(this.tileproperty[i].wallLeft);
				out.writeBoolean(this.tileproperty[i].wallRight);
			}
			
			out.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	    
	    return true;
	}
}
