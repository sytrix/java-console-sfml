package game.data;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import game.ressources.TileData;

public class TileMapData {
	
	private TileData[][] tilemap;
	private int width;
	private int height;
	private int defaultId;
	
	public TileMapData() {
		this.tilemap = null;
		this.width = 0;
		this.height = 0;
		this.defaultId = 0;
	}
	
	public void create(int w, int h, int id) {
		this.tilemap = new TileData[w][];
		
		for(int x = 0; x < w; x++) {
			this.tilemap[x] = new TileData[h];
			for(int y = 0; y < h; y++) {
				TileData tile = new TileData();
				tile.id = id;
				this.tilemap[x][y] = tile;
			}
		}
		
		this.defaultId = id;
		this.width = w;
		this.height = h;
	}
	
	public boolean exist() {
		if(this.tilemap == null) {
			return false;
		}
		
		return true;
	}
	
	public boolean changeTile(int x, int y, int id) {
		if(x < 0 || y < 0 || x >= this.width || y >= this.height) {
			return false;
		}
		
		this.tilemap[x][y].id = id; 
		
		return true;
	}
	
	public int getWidth() {
		return this.width;
	}
	
	public int getHeight() {
		return this.height;
	}
	
	public int getDefaultId() {
		return this.defaultId;
	}

	public TileData[][] getTileMap() {
		return this.tilemap;
	}

	public int getTile(int x, int y) {
		return this.tilemap[x][y].id; 
	}
	
	public boolean load(String filepath) {
		
		Path path = Paths.get(filepath);
		
		if(!Files.exists(path)) {
			return false;
		}
		
		DataInputStream in = null;
		
		try {
			in = new DataInputStream(new FileInputStream(filepath));
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return false;
		}
		
		try {
			int defaultId = in.readInt();
			int width = in.readInt();
			int height = in.readInt();
			
			this.create(width, height, defaultId);
			
			for(int x = 0; x < width; x++) {
				for(int y = 0; y < height; y++) {
					this.tilemap[x][y].id = in.readShort();
				}
			}
			
			in.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	    
	    
	    return true;
	}
	
	public boolean save(String filepath) {
		
		DataOutputStream out = null;
		
		try {
			out = new DataOutputStream(new FileOutputStream(filepath));
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return false;
		}
		
		try {
			out.writeInt(this.defaultId);
			out.writeInt(this.width);
			out.writeInt(this.height);
			
			for(int x = 0; x < this.width; x++) {
				for(int y = 0; y < this.height; y++) {
					out.writeShort(this.tilemap[x][y].id);
				}
			}
			
			out.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	    
	    return true;
	}
}

