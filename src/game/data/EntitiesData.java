package game.data;

import java.util.ArrayList;
import java.util.List;

import game.ressources.EntityData;

public class EntitiesData {
	private List<EntityData> entites;
	
	public EntitiesData() {
		this.entites = new ArrayList<>();
		
	}
	
	public void add(EntityData entity) {
		this.entites.add(entity);
	}
	
	public List<EntityData> getList() {
		return this.entites;
	}
}
